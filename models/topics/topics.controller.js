const express = require('express'),
      router = express.Router(),
      newsService = require('./topics.service')

//routes
router.post('/addTopics', addTopics)
router.get('/', getAll)
router.get('/:id', getById)
router.put('/:id', update)
router.delete('/:id', _delete)

module.exports = router

function addTopics(req,res,next){
    newsService.create(req.body)
    .then(() => res.status(201).json({message: 'Topics Inserted'}))
    .catch(err => res.status(202).json({code: 202, message: 'Topic alrady exist'}))
    // .catch(err => next(err)) 
}

function getAll(req,res, next){
    newsService.getAll()
    .then(users => res.json(users))
    .catch(err => next(err))
}

function getById(req,res,next){
    newsService.getById(req.params.id)
    .then(user => user ? res.json(user) : res.status(400))
    .catch(err => next(err))
}

function update(req,res,next){
    newsService.update(req.params.id,req.body)
    .then(() => res.status(201).json({code: 201, message: 'Topics Updated'}))
    .catch(err => res.status(202).json({code: 202, message: 'Topics Already Exist'}))
}

function _delete(req,res,next){
    newsService.delete(req.params.id)
    .then(() => res.status(201).json({code: 201, message: 'Topics Deleted'}))
    .catch(err => res.status(202).json({code: 202, message: 'Topics Not Found'}))
}