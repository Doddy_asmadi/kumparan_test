const config = require('../../config.json'),
      jwt = require('jsonwebtoken'),
      db = require('../../_helpers/db'),
      Topic = require('../topics/topics.model');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
}

async function getAll() {
    return await Topic.find().select('-hash')
}

async function getById(id){
    return await Topic.findById(id).select('-hash')
}

async function create(newsParam){
    const topics = new Topic(newsParam)
    await topics.save()
}

async function update(id, newsParam){
    const topics = await Topic.findById(id)
    if(!topics) throw 'Topic not found'
    Object.assign(topics,newsParam)
    await topics.save()
}

async function _delete(id){
    const topics = await Topic.findById(id)
    if(!topics) throw 'Topic not found'
    await Topic.findOneAndDelete(id)
}