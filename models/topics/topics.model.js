const mongoose = require('mongoose'),
      Schema = mongoose.Schema

const schema = new Schema({
    topic: {
        type: String, required: true
    },
    news: {
        type: String, required: true
    },
    createDate: {
        type: Date, default: Date.now
    }
})

schema.set('toJSON', {
    virtuals: true
})

module.exports = mongoose.model('Topic', schema)