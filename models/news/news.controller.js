const express = require('express'),
      router = express.Router(),
      newsService = require('./news.service')

//routes
router.post('/addNews', addNews) //insert
router.get('/', getAll) //get all
router.get('/:id', getById) //get by id
router.get('/status/:status', getByStatus) //get by status
router.get('/topic/:topic', getByTopic) //get by topic
router.put('/:id', update) //update by id
router.delete('/:id', _delete) //delete by id

module.exports = router

function addNews(req,res,next){
    newsService.create(req.body)
    .then(() => res.status(201).json({message: 'News Inserted'}))
    .catch(err => next(err)) 
}

function getAll(req,res, next){
    newsService.getAll()
    .then(users => res.json(users))
    .catch(err => next(err))
}

function getById(req,res,next){
    newsService.getById(req.params.id)
    .then(user => user ? res.json(user) : res.status(400))
    .catch(err => next(err))
}

function getByStatus(req,res,next){
    newsService.getByStatus(req.params)
    .then(user => user ? res.json(user) : res.status(400))
    .catch(err => next(err))
}

function getByTopic(req,res,next){
    newsService.getByTopic(req.params)
    .then(user => user ? res.json(user) : res.status(400))
    .catch(err => next(err))
}

function update(req,res,next){
    newsService.update(req.params.id,req.body)
    .then(() => res.status(201).json({code: 201, message: 'News Updated'}))
    .catch(err => res.status(202).json({code: 202, message: 'News Already Exist'}))
}

function _delete(req,res,next){
    newsService.delete(req.params.id)
    .then(() => res.status(201).json({code: 201, message: 'News Deleted'}))
    .catch(err => res.status(202).json({code: 202, message: 'News Not Found'}))
}