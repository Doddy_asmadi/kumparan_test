const News = require('../news/news.model');

module.exports = {
    getAll,
    getById,
    getByStatus,
    getByTopic,
    create,
    update,
    delete: _delete,
}

async function getAll() {
    return await News.find().select('-hash')
}

async function getById(id){
    return await News.findById(id).select('-hash')
}

async function getByStatus(newsParam){
    return await News.find({status: newsParam.status}).select('-hash')
}

async function getByTopic(newsParam){
    return await News.find({topic: newsParam.topic}).select('-hash')
}

async function create(newsParam){
    const news = new News(newsParam)
    await news.save()
}

async function update(id, newsParam){
    const news = await News.findById(id)
    if(!news) throw 'News not found'
    Object.assign(news,newsParam)
    await news.save()
}

async function _delete(id){
    const news = await News.findById(id)
    if(!news) throw 'News not found'
    await News.findOneAndDelete(id)
}