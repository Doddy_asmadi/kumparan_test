const mongoose = require('mongoose'),
      Schema = mongoose.Schema

const schema = new Schema({
    title: {
        type: String, required: true
    },
    topic: {
        type: String, required: true
    },
    status: {
        type: String, required: true
    },
    createDate: {
        type: Date, default: Date.now
    }
})

schema.set('toJSON', {
    virtuals: true
})

module.exports = mongoose.model('News', schema)