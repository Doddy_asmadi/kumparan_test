const express = require('express'),
      app = express(),
      cors = require('cors'),
      bodyParser = require('body-parser'),
    //   jwt = require('./_helpers/jwt'),
      errorHandler = require('./_helpers/error-handler'),
      db = require('./_helpers/db');

app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(bodyParser.json())
app.use(cors())

//use jwt to secure api
// app.use(jwt())

app.use(express.static(__dirname + '/public'));

//api routes
app.use('/news', require('./models/news/news.controller'))
app.use('/topic', require('./models/topics/topics.controller'))

//global error handler
app.use(errorHandler)

//start server
// const port = process.env.NODE_ENV === 'production' ? 80 : 4000
const port = process.env.PORT;
const server = app.listen(port, function(){
    console.log('Server listening on port '+ port)
})