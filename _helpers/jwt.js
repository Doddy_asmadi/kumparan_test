const expressJwt = require('express-jwt'),
      config = require('../config.json');
    //   userService = require('../users/user.service');

module.exports = jwt;

function jwt(){
    const secret = config.secret
    return expressJwt({
        secret, isRevoked
    }).unless({
        path: [
            //routes that don't require authentication
            '/users/authenticate',
            '/news/add',
            '/news',
        ]
    })
}

async function isRevoked(req, payload,done){
    console.log(payload)
    // const user = await userService.getById(payload.sub)

    // //revoke token if user not exist
    // if(!user){
    //     return done(null, true)
    // }
    // done()
}